Hickok I-177 Technical Manuals
------------------------------

- [1944.pdf](https://calebreister.gitlab.io/hickok_i177/1944.pdf) (3 August 1944): Original I-177 Technical Manual
- [1945.pdf](https://calebreister.gitlab.io/hickok_i177/1945.pdf) (24 October 1945): RESTRICTED classification removed, revisions to Section IV (Maintenance)
- [1949.pdf](https://calebreister.gitlab.io/hickok_i177/1949.pdf) (1 August 1949): Revision to document the I-177-A
- [1954.pdf](https://calebreister.gitlab.io/hickok_i177/1954.pdf) (5 March 1954): Revision to document the I-177-B
- [1955.pdf](https://calebreister.gitlab.io/hickok_i177/1955.pdf) (26 October 1955): Minor revision to Quality Test section
- [I-177_Schematic.pdf](https://calebreister.gitlab.io/hickok_i177/i-177_schematic.pdf): Schematic for the original I-177 tube tester (Figure 13 in the 1944 manual)
- [I-177-A_Schematic.pdf](https://calebreister.gitlab.io/hickok_i177/i-177-a_schematic.pdf): Schematic for the I-177-A
- [I-177-B_Schematic.pdf](https://calebreister.gitlab.io/hickok_i177/i-177_schematic.pdf): Schematic for the I-177-B
