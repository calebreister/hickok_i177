# Source files
PAGES := $(wildcard scan/*.pgm)
YEARS := 1944 1945 1949 1954 1955

# Unpaper configuration
UNPAPER = unpaper -q --overwrite --no-grayfilter $(UNPAPER_FLAGS) $< $@
UNPAPER_PAGES := $(PAGES:scan/%.pgm=unpaper/%.pgm)
UNPAPER_SKIP  := scan/1945_Page_14.pgm  \
				 scan/1949_Page_03.pgm  \
		         scan/1949_Page_04.pgm  \
		         scan/1949_Page_05.pgm  \
				 scan/1949_Page_06A.pgm \
				 scan/1954_Page_04A.pgm 

# Tesseract configuration
export OMP_THREAD_LIMIT := 1
TESSERACT_FLAGS := --dpi 300 --oem 2
OCR_PAGES := $(UNPAPER_PAGES:unpaper/%.pgm=ocr/%.pdf)
SECTION = sed -nr '/TABLE OF CONTENTS$$/p; \
				   /(SECTION\s+[IV]+)$$/ {s/$$/ - /; N; N; s/\n+//p}' \
				   $(@:.pdf=.txt)

.PHONY: all unpaper ocr
all: $(foreach y, $(YEARS), $y.pdf) \
	 I-177_Schematic.pdf   \
	 I-177-A_Schematic.pdf \
	 I-177-B_Schematic.pdf
unpaper: $(UNPAPER_PAGES)
ocr: $(OCR_PAGES)

# Run unpaper
# - Files listed in UNPAPER_SKIP are copied directly
# - Unpaper warnings have been suppressed
unpaper/1944_Front_III.pgm: UNPAPER_FLAGS := -mt 0.05
unpaper/1944_Page_15.pgm:   UNPAPER_FLAGS := -mt 0.05
unpaper/%_Cover.pgm:        UNPAPER_FLAGS := -mt 0.05
unpaper/%.pgm: scan/%.pgm
	$(if $(filter $<,$(UNPAPER_SKIP)), cp $< $@, $(UNPAPER) 2> /dev/null)

# Run OCR (tesseract)
ocr/%.pdf ocr/%.txt &: unpaper/%.pgm
	tesseract $(TESSERACT_FLAGS) $< $(basename $@) pdf txt quiet
	mv $@ $@~
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ \
	   -c "[/Page 1 /Title ($$($(SECTION))) /OUT pdfmark" \
	   -f $@~

# Generate ocr/${YEAR}_Front.pdf with correct page numbering
define FRONT_PDF_RULE
ocr/$(1)_Front.pdf: ocr/$(1)_Cover.pdf $(filter ocr/$(1)_Front_%.pdf, $(OCR_PAGES))
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $$@ \
	   -c "[/_objdef {pl} /type /dict /OBJ pdfmark" \
       -c "[{pl} <</Nums [0 <</P (cover)>> 1 <</S/R>> $$(words $$^) <</S/D>>]>>" \
       -c " /PUT pdfmark [{Catalog} <</PageLabels {pl}>> /PUT pdfmark" \
       -f $$^
endef

# Generate ${YEAR}.pdf by merging the ocr/${YEAR}_Front.pdf and
# all pages
define MERGE_PDF_RULE
$(1).pdf: ocr/$(1)_Front.pdf $(filter ocr/$(1)_Page_%.pdf, $(OCR_PAGES)) \
		  pdfmarks/$(1).pdfmarks
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $$@ -f $$^
endef

# Generate PDF merging rules
$(foreach y, $(YEARS), $(eval $(call FRONT_PDF_RULE,$y)))
$(foreach y, $(YEARS), $(eval $(call MERGE_PDF_RULE,$y)))

# Schematic PDF files
I-177%Schematic.pdf: scan/I-177%Schematic.pgm pdfmarks/I-177%Schematic.pdfmarks
	convert $< $@
	mv $@ $@~
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ -f $@~ $(filter-out $<,$^)
	rm $@~

# Clean
.PHONY: clean clean-unpaper clean-ocr
clean: clean-unpaper clean-ocr
clean-unpaper:
	rm -f unpaper/*
clean-ocr:
	rm -f ocr/*
